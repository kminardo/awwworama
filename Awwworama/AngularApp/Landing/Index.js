﻿/// <reference path="../../Scripts/_references.js" />

angular.module('main')
    .controller('Landing.IndexController', ['$scope', '$http', function ($scope, $http) {
        $scope.loading = true;
        $scope.subreddit = "";

        $scope.fetchSubreddit = function (subreddit) {
            $scope.loading = true;
            $http.get('/api/RedditAPI/' + subreddit).success(function (posts) {
                $scope.posts = posts;
                $scope.loading = false;
            });
        }

        //Onload
        $scope.fetchSubreddit();
    }]);